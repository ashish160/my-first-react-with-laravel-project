<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\RoleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/employees')->group(function () {
        Route::get('/', [EmployeesController::class, 'index']);
        Route::post('/add-employee', [EmployeesController::class, 'addEmployee']);
        Route::post('/edit-employee', [EmployeesController::class, 'editEmployee']);
        Route::get('/get-employee/{id}', [EmployeesController::class, 'getEmployee']);
        Route::post('/delete-employee/{id}', [EmployeesController::class, 'deleteEmployee']);
    });

    Route::prefix('/roles')->group(function () {
        Route::get('/', [RoleController::class, 'index']);
        Route::post('/add-role', [RoleController::class, 'addRole']);
        Route::post('/edit-role', [RoleController::class, 'editRole']);
        Route::get('/get-role/{id}', [RoleController::class, 'getRole']);
        Route::post('/delete-role/{id}', [RoleController::class, 'deleteRole']);
    });
});
