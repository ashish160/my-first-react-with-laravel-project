// packages imported
import React from "react"
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom"
import { ToastContainer, toast } from 'react-toastify';
import ReactDOM from 'react-dom';

// components imported
import LoginPage from "../Login";
import SignUpPage from "../SignUp";
import DashboardPage from "../Dashboard";
import EmployeesPage from "../Employees";
import AddEmployees from "../Employees/AddEmployees"
import EditEmployees from "../Employees/EditEmployees"
import RolesPage from "../Roles"
import AddRoles from "../Roles/AddRoles"
import EditRoles from "../Roles/EditRoles"

const ProtectedRoute = ({component: Component, ...rest}) => {
    return <Route {...rest} render={props=> localStorage.getItem('token')?<Component {...props} /> : <Redirect to={"/login"} />} />
}
 
const PublicRoute = ({component: Component, ...rest}) => {
    return <Route {...rest} render={props=> !localStorage.getItem('token')?<Component {...props} /> : <Redirect to={"/login"} />} />
}

export default function App() {
return(
    <div className="h-100">
        <ToastContainer autoClose={3000}  />
        <BrowserRouter>
            <Switch>
                {/* Public-Routes accesible by everyone */}
                <PublicRoute exact path="/login" component={LoginPage} />
                <PublicRoute exact path="/sign-up" component={SignUpPage} />

                {/* Protected-Routes accessible by system users */}
                <ProtectedRoute exact path="/" component={DashboardPage} />
                    {/* Employees Management Routes */}
                    <ProtectedRoute exact path="/employees" component={EmployeesPage} />
                    <ProtectedRoute exact path="/employees/add-employees" component={AddEmployees} />
                    <ProtectedRoute exact path="/employees/edit-employees/:id" component={EditEmployees} />

                    {/* Roles Management Routes */}
                    <ProtectedRoute exact path="/roles" component={RolesPage} />
                    <ProtectedRoute exact path="/roles/add-roles" component={AddRoles} />
                    <ProtectedRoute exact path="/roles/edit-roles/:id" component={EditRoles} />
            </Switch>
        </BrowserRouter>
    </div>
    )
}

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}