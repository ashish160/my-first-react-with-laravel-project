import React, {useEffect, useMemo, useState} from 'react';
import { createBrowserHistory } from 'history';
import ApiClient from "../../config";
import {toast} from "react-toastify";
import { MDBCard, MDBCardBody, MDBCol, MDBContainer, MDBDataTable, MDBRow } from 'mdbreact';
import 'react-toastify/dist/ReactToastify.css';
import {
    PowerSettingsNewTwoTone as PowerOff, VisibilityOutlined as ViewRecord, EditOutlined as EditRecord, DeleteOutlineRounded as DeleteRecord, Add as AddEmployeesIcon
} from '@mui/icons-material';

import {Breadcrumbs, Link, Typography} from '@material-ui/core'

export default function Employees() {
    let history = createBrowserHistory({forceRefresh:true});
    const [userData, setUserData] = useState([]);
    const userEmail = localStorage.getItem('users');
    
    const logOut = () => {
        if(confirm('Do you want to log out?')) {
            localStorage.removeItem('users')
            localStorage.removeItem('token')
            localStorage.removeItem('roleName')
            return history.push("/login")
        }
    }

    const deleteEmp = (id) => {
        if (confirm('Are you sure you want to delete this employee?')) {
            ApiClient.post('/api/employees/delete-employee/' + id)
            .then(response => {
                if(response.data){
                    toast.success('Record deleted successfully')
                    return history.push("/employees")
                }
            });
        }else{
            return
        }
    }

    useMemo(()=>{
        ApiClient.get('/api/employees/')
        .then(response => {
            console.log("response =>\n", response)
            let postsArray = [];
            JSON.parse(JSON.stringify(response.data)).map((item, index) => {
                item.role = (
                    <div className='mx-1 rounded custom-actions'>
                        {item.roleName}
                    </div>
                );
                item.actions = (
                    <div className='text-center d-flex'>
                        <div className='mx-1 rounded custom-actions'>
                            <ViewRecord className='action-icon-style' onClick={()=>{alert(item.id)}}/>
                        </div>
                        <div className='mx-1 rounded custom-actions'>
                            <EditRecord className='action-icon-style' onClick={() => {history.push(`/employees/edit-employees/${item.id}`)}}/>
                        </div>
                        <div className='mx-1 rounded custom-actions'>
                            <DeleteRecord className='action-icon-style' onClick={()=>{deleteEmp(item.id)}} /> 
                        </div>
                    </div>  
                );
                postsArray.push(item);
            });
            setUserData(postsArray)
        });
    },[userEmail]);
    
    const data = {
        columns: [
            {
                label: 'ID',
                field: 'id',
                sort: 'asc',
                width: 150,
                attributes: {className: 'text-center'}
            },
            {
                label: 'First Name',
                field: 'first_name',
                sort: 'asc',
                width: 150
            },
            {
                label: 'Last Name',
                field: 'last_name',
                sort: 'asc',
                width: 150
            },
            {
                label: 'Email',
                field: 'email',
                sort: 'asc',
                width: 270
            },
            {
                label: 'Role',
                field: 'role',
                sort: 'asc',
                width: 100,
                attributes: {className: 'text-center'}
            },
            {
                label: 'Actions',
                field: 'actions',
                width: 100,
                attributes: {className: 'text-center'}
            }
        ],
        rows: userData
    };

    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <h4 className="main-header-style"><strong>Broadweb Digital HR Management System</strong></h4>
                    <div className="row">
                        <div className='col-md-6'>
                            <Breadcrumbs aria-label="breadcrumb">
                                <Link underline="hover" color="inherit" href="/">
                                    Dashboard
                                </Link>
                                <Typography className='text-black'>Employees</Typography>
                            </Breadcrumbs>
                        </div>
                        <div className='col-md-6'>
                            <PowerOff className="svg-logout-style" onClick={logOut}/>
                            <span className='text-align-right role-style'><strong><small>(Role: {localStorage.getItem('roleName')})</small></strong></span> 
                        </div>
                    </div>
                    <div className="card mt-3">
                        <div className="card-header">
                            <strong>Employee Details</strong>
                            <AddEmployeesIcon className='text-align-right add-icon-style' onClick={()=>{history.push('/employees/add-employees')}}/>    
                        </div>
                        <div className="card-body">
                            <MDBDataTable 
                                className='empTable'
                                striped 
                                bordered 
                                small
                                entries={5}
                                data = {data}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
