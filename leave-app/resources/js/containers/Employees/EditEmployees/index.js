import {React, useState, useEffect, useMemo} from 'react';
import { createBrowserHistory } from 'history';
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import ApiClient from "../../../config";
import EmailRegex from "../../../components/regex-patterns";
import {
    PowerSettingsNewTwoTone as PowerOff
} from '@mui/icons-material';
import {Breadcrumbs, Link, Typography} from '@material-ui/core'

export default function EditEmployees({match}) {
    const {params:{id}} = match
    let history = createBrowserHistory({forceRefresh:true});
    const [user, setUser] = useState({
        id:"",
        first_name: "",
        last_name: "",
        email: ""
    });
    const {first_name, last_name, email} = user;
    const onInputChange = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value });
    };
    
    const sendEmpData = () =>
    {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(user.first_name === ''){
            return toast.error('First Name Field is empty')
        }else if(user.last_name === ''){
            return toast.error('Last Name Field is empty')
        }else if(user.email === ''){
            return toast.error('Email Field is empty')
        }else if(!regex.test(String(user.email).toLowerCase())){
            return toast.error('Enter valid email')
        }

        ApiClient.post('/api/employees/edit-employee/',user)
        .then(response => {
            if(response.data){
                toast.success("Employee Edit Successfully")
                history.push('/employees')// To Clear all fields
            }else{
                toast.error('Something went wrong try again')
            }
        });
    }

    const logOut = () => {
        if(confirm('Do you want to log out?')) {
            localStorage.removeItem('users')
            localStorage.removeItem('token')
            localStorage.removeItem('roleName')
            return history.push("/login")
        }
    }

    useEffect(() => {
        if(id){
            ApiClient.get('/api/employees/get-employee/' + id)
            .then(response => {
                if(response.data){
                    // Data set to useState - (user)
                    setUser({first_name:response.data.first_name, last_name:response.data.last_name, email:response.data.email, id: response.data.id}) 
                }else{
                    toast.error('Something went wrong try again')
                }
            });
        }
    }, [id]);
    
    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <h4 className="main-header-style">Broadweb Digital HR Management System</h4>
                    <div className="row">
                        <div className='col-md-6'>
                            <Breadcrumbs aria-label="breadcrumb">
                                <Link underline="hover" color="inherit" href="/">
                                    Dashboard
                                </Link>
                                <Link underline="hover" color="inherit" href="/employees">
                                    Employees
                                </Link>
                                <Typography className='text-black'>Edit Employees</Typography>
                            </Breadcrumbs>
                        </div>
                        <div className='col-md-6'>
                            <PowerOff className="svg-logout-style" onClick={logOut}/>
                            <span className='text-align-right role-style'><strong><small>(Role: {localStorage.getItem('roleName')})</small></strong></span> 
                        </div>
                    </div>
                    <div className="card mt-3">
                        <div className="card-header">
                            <strong>Edit Employee</strong>
                        </div>
                        <div className="card-body">
                            <div className="form-group">
                                <input type="text" className="form-control" label="First Name" name="first_name" value={first_name} onChange={e => onInputChange(e)} placeholder="Enter First Name" required/>
                            </div>
                            <div className="form-group mt-2">
                                <input type="text" className="form-control" label="Last Name" name="last_name" value={last_name} onChange={e => onInputChange(e)} placeholder="Enter Last Name" required/>
                            </div>
                            <div className="form-group mt-2">
                                <input type="email" className="form-control" label="Email" name="email" value={email} onChange={e => onInputChange(e)} placeholder="Enter Email" required/>
                            </div>
                            <hr/>
                            <button type="submit" className="btn btn-primary" onClick={sendEmpData}>Submit</button>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
