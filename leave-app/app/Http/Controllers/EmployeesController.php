<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class EmployeesController extends BaseController
{
    public function index()
    {
        try{
            $employees = User::get();
            foreach ($employees as $key => $user) {
                $employees[$key]['roleName'] = $user->getRoleNames();
            }
            return response()->json($employees);
        }catch (\Exception $e){
            return $this->sendError('something went wrong', [], 500);
        }
    }
    
    /**
     * @param $red
     * @return JsonResponse
     */
    public function addEmployee(Request $req)
    {
        try{
            $user = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'password'=> Hash::make($req->password)
            ]);
            return $this->sendResponse($user, 'User login successfully.', 200);
        }catch (\Exception $e){
            return $this->sendError('something went wrong', [], 500);
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getEmployee($id){
        try{
            $user = User::find($id);
            return response()->json($user);
        }catch (\Exception $e){
            return $this->sendError('something went wrong', [], 500);
        }
    }

    /**
     * @param $red
     * @return JsonResponse
     */
    public function editEmployee(Request $req)
    {
        try{
            $user = User::updateOrCreate(['id'=>$req->id],[
                'email'=>$req->email,
                'name'=>$req->name
            ]);
            return $this->sendResponse($user, 'Data updated successfully.', 200);
        }catch (\Exception $e){
            return $this->sendError('something went wrong', [], 500);
        }
    }

    /**
     * @param $red
     * @return JsonResponse
     */
    public function deleteEmployee($id)
    {
        try{
            $user = User::where('id', $id)->delete();
            return $this->sendResponse($user, 'User deleted successfully.', 200);
        }catch (\Exception $e){
            return $this->sendError('something went wrong', [], 500);
        }
    }
    
}
